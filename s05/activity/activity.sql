SELECT customerName FROM customers WHERE country = "Philippines";
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";
SELECT productName, MSRP FROM products WHERE productName LIKE "%The Titanic";
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";
SELECT customerName FROM customers WHERE state IS NULL;
SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;
SELECT orderNumber FROM orders WHERE comments LIKE "%dhl%";
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";
SELECT DISTINCT country FROM customers;
SELECT DISTINCT status FROM orders;
SELECT customerName, country FROM customers WHERE country IN ("USA","France","Canada");
SELECT employees.firstName, employees.lastName, offices.city FROM offices JOIN employees ON offices.officeCode = employees.officeCode WHERE offices.city = "Tokyo";
SELECT customers.customerName FROM employees JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber WHERE employees.firstName = "Leslie" AND employees.lastName = "Thompson";
SELECT products.productName FROM customers JOIN orders ON customers.customerNumber = orders.customerNumber JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber JOIN products ON orderdetails.productCode = products.productCode WHERE customers.customerName = "Baane Mini Imports";
SELECT employees.firstName, employees.lastName, customers.customerName,offices.country FROM customers JOIN employees ON employees.employeeNumber = customers.salesRepEmployeeNumber JOIN offices ON offices.officeCode = employees.officeCode WHERE customers.country = offices.country;
SELECT products.productName, products.quantityInStock FROM products JOIN productlines ON products.productline = productlines.productline WHERE productlines.productline = "planes" AND products.quantityInStock < 1000;
SELECT customerName FROM customers WHERE phone LIKE "%+81%";