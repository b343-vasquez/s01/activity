-- [SECTION] Advance Selects

-- Exclude records
SELECT * FROM songs WHERE song_name != "Kundiman"

-- Exlude OPM songs and we want to show the song_name and length column only.

SELECT song_name, length FROM songs WHERE genre != "opm";

SELECT song_name, length FROM songs WHERE genre != "OPM";

-- Greater than or equal && Less than or equal

SELECT * FROM songs WHERE length > 300;

SELECT * FROM songs WHERE length < 300;

-- Songs that are longer than 3 min but shorter than 4 mins

SELECT * FROM songs WHERE length > 300 AND length < 400;

SELECT * FROM songs WHERE length >= 300 AND length <= 416;

-- OR operator
SELECT * FROM songs WHERE id = 1 OR id = 5 OR id = 3;

-- IN OPERATOR
SELECT * FROM songs WHERE id IN (1,3,5);
SELECT * FROM songs WHERE genre IN ("Pop", "Pop rock");

-- FIND partial matches:
SELECT * FROM songs WHERE song_name LIKE "%y";
SELECT * FROM songs WHERE song_name LIKE "%Story";

SELECT * FROM songs WHERE song_name LIKE "k%";

SELECT * FROM songs WHERE song_name LIKE "s%e";

SELECT * FROM songs WHERE song_name LIKE "%r%";

SELECT * FROM songs WHERE song_name LIKE "%s%o%";

SELECT * FROM songs WHERE song_name LIKE "%s%o%" OR song_name LIKE "%o%s%";

SELECT * FROM songs WHERE song_name LIKE "l%r%";

SELECT * FROM songs WHERE song_name LIKE "l%r%" AND genre = "Pop";
-- LIKE (%) (Keyword)
-- LIKE (_) (Pattern) Exact number of characters

SELECT * FROM albums WHERE album_title LIKE "Tr_";
SELECT * FROM albums WHERE album_title LIKE "Tr_p"; --(4letters)

SELECT * FROM albums WHERE album_title LIKE "Tr%";

SELECT * FROM songs WHERE id LIKE "1_"; 
SELECT * FROM songs WHERE id LIKE "_"; -- single digit
SELECT * FROM songs WHERE id LIKE "__"; -- double digit

-- Sorting records:
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;

SELECT * FROM songs WHERE song_name LIKE "%s%o%" ORDER BY song_name ASC;

-- Get Distinct records
SELECT DISTINCT genre FROM songs;


-- [SECTION] Table Joins - (Tables that have relationship)
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id;

SELECT artists.name, albums.album_title FROM artists JOIN albums ON artists.id = albums.artist_id;

SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

SELECT artists.name, albums.album_title, songs.song_name FROM artists JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

SELECT artists.name, albums.album_title, songs.song_name FROM artists JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id ORDER BY song_name ASC;

SELECT artists.name, albums.album_title, songs.song_name FROM artists JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id 
	WHERE song_name LIKE "l%"
	ORDER BY song_name ASC;